#!/bin/bash

# ==================================#
# Autoscale infrastructure creation #
# ==================================#
#
# Script to automate the setup of Rackspace Autoscale
# using rax-autoscaler script https://github.com/rackerlabs/rax-autoscaler
# It uses the dev branch
#
# Author - Chris (thtieig)
#

clear
echo 'This script has been designed to run on Centos 7 - 1GB Server.
At the end of this, your server will be the rax-autoscale ADMIN node.'

echo 'We do recommend to have this server set in your DNS.'

echo '------------------------'
echo 'Gathering information'
echo '------------------------'
read -p "Customer's user name: " USERNAME
read -p "Customer's account ID: " USERID
read -p "Customer's API KEY: " APIKEY
read -p "Region: " REGION1
read -p "Load Balancer(s) ID
If more than one, comma separated [e.g. 254784, 854574]: " LBS

REGION=$(echo $REGION1 | tr '[:lower:]' '[:upper:]')

echo -e "\nThis is what I've got:
Username: $USERNAME
Account ID: $USERID
API KEY: $APIKEY
Region: $REGION
CLB ID: $LBS

All good? Happy? If not, press CTRL + C, otherwise, press any key to continue..."
read voidvar

clear

#=============================================================
# ALL stdout and stderr to $LOG_OUTPUT
# Also stderr to $LOG_ERROR (for extra checks)
# P function to print to the console AND logged into $LOG_OUTPUT
LOG_OUTPUT=autoscale-setup_output.log
LOG_ERROR=autoscale-setup_error.log

exec 3>&1 1>>${LOG_OUTPUT}
exec 2> >(tee -i ${LOG_ERROR}) >> ${LOG_OUTPUT} 

# use 'P "my message"' instead of echo to print in BLUE
P () {
BLUE='\033[1;34m'
NC='\033[0m' # No Color
echo -e "\n${BLUE}${1}${NC}" | tee /dev/fd/3 
}

#=============================================================
get_token () {
TOKEN=$(
curl -sd \
"{
   \"auth\":
   {
        \"RAX-KSKEY:apiKeyCredentials\":
        {\"username\": \"$USERNAME\",
        \"apiKey\": \"$APIKEY\"}        }
}" \
-H 'Content-Type: application/json' \
'https://identity.api.rackspacecloud.com/v2.0/tokens' | python -m json.tool | grep -A 5 token | awk '/id/ { print $2 }' | tr -d '"' | tr -d ","
)
export TOKEN=$TOKEN
}

inst_autoscale-setup () {
git clone https://github.com/eljrax/autoscale_setup
cd autoscale_setup/
pip install -r requirements.txt
}


inst_rax-autoscaler () {
pip install rax-autoscaler
mkdir -p /etc/rax-autoscaler/ && cp /opt/autoscale/rax-autoscaler/config/logging.conf /etc/rax-autoscaler/
mkdir -p /var/log/rax-autoscaler/
#For legacy purposes, create a symlink to the new config file
touch /opt/autoscale/autoscale_setup/rax-autoscaler-config.json
ln -sf /opt/autoscale/autoscale_setup/rax-autoscaler-config.json /opt/autoscale/rax-autoscaler/config/config.json
ln -sf /opt/autoscale/autoscale_setup/rax-autoscaler-config.json  /etc/rax-autoscaler/config.json
}

inst_rax-autoscaler_OLD () {
cd /opt/autoscale
git clone https://github.com/eljrax/rax-autoscaler.git
cd rax-autoscaler
git fetch
git checkout devel
pip install -r requirements.txt
touch README.txt
pip install --upgrade requests
pip install setuptools --upgrade
python setup.py build
python setup.py install
mkdir -p /etc/rax-autoscaler/ && cp /opt/autoscale/rax-autoscaler/config/logging.conf /etc/rax-autoscaler/
mkdir -p /var/log/rax-autoscaler/
#For legacy purposes, create a symlink to the new config file
touch /opt/autoscale/autoscale_setup/rax-autoscaler-config.json
ln -sf /opt/autoscale/autoscale_setup/rax-autoscaler-config.json /opt/autoscale/rax-autoscaler/config/config.json
ln -sf /opt/autoscale/autoscale_setup/rax-autoscaler-config.json  /etc/rax-autoscaler/config.json
}

fix_autoscale_py () {
sed -i "s/^load_average.*$/load_average = \(1\.0\, 2\.0\)/" /opt/autoscale/payload/autoscale.py
sed -i "s/^num_conns.*$/num_conns = \(20\, 100\)/" /opt/autoscale/payload/autoscale.py
sed -i "s/^memory_pct_used.*$/memory_pct_used = \(50\.0\, 75\.0\)/" /opt/autoscale/payload/autoscale.py
}

autoscale_setup_defaults () {
cat > /opt/autoscale/autoscaler.ini <<EOF
[launch-configuration]
cloud_init = 'templates/cloud-init.yml.j2'
disk_config = 'MANUAL'
skip_default_networks = False


[rax-autoscaler]
load_balancers = [$LBS]
private_key = '/opt/autoscale/.ssh/cloudinitsshkey.pub'

[cloud]
credentials_file = '/opt/autoscale/.credentials'
EOF

mv /opt/autoscale/autoscale_setup/templates/cloud-init.yml.j2 /opt/autoscale/autoscale_setup/templates/cloud-init.yml.j2.ORIG
cat > /opt/autoscale/autoscale_setup/templates/cloud-init.yml.j2 <<EOF
#cloud-config

packages:
  - python-pip
  - python-dev
  - gcc

write_files:
  - path: /root/.ssh/id_rsa
    permissions: 0600
    owner: root:root
    encoding: b64
    content: {{ private_key }} 

  - content: |
        #!/bin/bash
        until [[ -f /var/lib/cloud/instance/boot-finished ]]; do
          sleep 5
        done
        echo "Starting setup.sh script" | logger
        pip install ansible==1.9.4 pyrax
        echo -e "[defaults]\nlog_path=/var/log/ansible.log" > /root/.ansible.cfg
        ansible-playbook -i /opt/autoscale/playbook/hosts /opt/autoscale/playbook/site.yml --connection=local
        mkdir -p /usr/lib/rackspace-monitoring-agent/plugins/
        cp /opt/autoscale/autoscale.py /usr/lib/rackspace-monitoring-agent/plugins/autoscale.py
        chmod 700 /usr/lib/rackspace-monitoring-agent/plugins/autoscale.py
        chmod 700 /opt/autoscale/add_self_to_lb.py
	# check - if fails => destroy and re-create
	# [ -f /var/www/vhosts/testfile ] && [ -f /mnt/share/mounted ] && /opt/autoscale/add_self_to_lb.py && rm -f /opt/autoscale/.credentials || bash /opt/autoscale/auto_kill.sh
        /opt/autoscale/add_self_to_lb.py
        rm -f /opt/autoscale/.credentials
        echo "Ending setup.sh script" | logger
    path: /root/setup.sh

runcmd:
  - rsync -ave "ssh -o StrictHostKeyChecking=no" autoscale@{{ admin_server }}:/opt/autoscale/payload/ /opt/autoscale/
  - rsync -ave "ssh -o StrictHostKeyChecking=no" autoscale@{{ admin_server }}:/opt/autoscale/.credentials /opt/autoscale/.credentials
  - nohup /bin/bash /root/setup.sh &
EOF

}

extra_help () {
cat >/opt/autoscale/autoscale_setup/extra_help_for_me.txt<<EOF

===============================================
MAIN.PY

The file main.py generates multiple config files and its able to find changes and re-apply them.
In specific, all the parameters that you will set on the first time can be deleted one by one and re-added simply re-running this file.
Main configuration file is in /opt/autoscale/autoscaler.ini

Example:
---------

Select group <Create new>

Name of autoscale_group: <This is the name of your autoscale group>

Number of servers to scale up by when triggered: 1 <you can start with 1 and change it later>

Number of servers to scale down by when triggered: 1 <same as above>

Max number of servers to scale up to (max_entities): 3 <same as above>

Never scale down below this number of servers (min_entities): 1 <same as above>

Do not process scale event more frequent than this (cooldown, seconds): 30 <30 sec but better to increase once in production>

Select image: <I would suggest CentOS 7>
40 - CentOS 7 (PVHVM) (6595f1b7-e825-4bd2-addc-c7b1c803a37f)

Select flavor: 13 <1GB at least to start>
13 - 1 GB General Purpose v1 (general1-1)

Select ssh-key to add to /root/.ssh/authorized_keys on the servers:
1- Create new

Path to public key file: /opt/autoscale/.ssh/id_rsa.pub

Keypair name: <name that will be visible under SSH keys on the customer's control panel>

Server name (note that an 11 character suffix will be added to this name): <this is the prefix of the autoscale servers. I would use 'web'>

Path to cloud-init script: templates/cloud-init.yml.j2

Supply one or more networks you wish to attach the cloud servers to <if none, just select 'Done'>

Keep default networks? (y/n): y

Disk config method (AUTO or MANUAL): AUTO

Supply one or more load balancers you wish to attach the cloud servers to
Select load balancer: <if you want the servers to do pre-checks against the Load Balancer's health check, select 'Done'>

Private key to inject into /root/.ssh/id_rsa on servers: /opt/autoscale/.ssh/cloudinitsshkey

IP or host-name of admin server to download playbook from: <if you have your admin node set in the DNS, use the full FQND, alternatively, use the PUBLIC IP>

===============================================
ADD_SELF_TO_LB and REMOVE_DEAD_NODES

The scripts 'add_self_to_lb.py' and 'remove_dead_nodes.py' are used ONLY if you don't specify a CLB during this setup. This is generally required if you have multiple load balancers or if you build your servers via playbook and you want to be sure that all the setup is completed before the server gets added under the CLB. By default, the server gets added as soon as it got marked 'Active' which doesn't really mean that the setup has been completed and/or the code fully deployed.

I would suggest to setup the autoscale group without the load balancer and use 'add_self_to_lb.py' as last step in your cloud-init file and have 'remove_dead_nodes.py' enabled in 'autoscale' user's cron. This last script clean up the CLB(s) from the deleted nodes.
Not setting the CLB in the autoscale group won't automatically remove the nodes from the CLB and we need some manual intervention to be sure no dead references are kept.

===============================================
LAUNCH_CONFIGURATION
/opt/autoscale/autoscale_setup/launch_configuration.py

When a server gets built via autoscale, some metadata will inform our automation about what to pre-setup (e.g. backup agent, monitoring, rack user etc...).
By default, our Managed servers are getting Backup and Monitoring agents installed and this is configured bia "build_config" metadata options.

At this stage, we have seen that for pure Managed accounts, the following line seems working fine (adds only 'rack' user and installs the monitoring agent:
metadata = {'rax-autoscaler-setup': '1.0', 'build_config': 'monitoring_agent_only,rack_user_only'}

Same as creating a server via 'nova' using this --meta build_config=monitoring_agent_only,rack_user_only

Another option could be using 'build_config_v2'. At the moment there is only 'support_user' option available, but it seems the ONLY way to have the 'rack' user created bypassing the 'RackConnect' meta tag, present also in those accounts with RackConnect automation disabled.
To achieve this, here the line:
metadata = {'rax-autoscaler-setup': '1.0', 'build_config_v2': 'support_user'}

==============================================
CLOUD INIT

We are using this file to run some commands onto the new servers created by the autoscale group.

~ vim /opt/autoscale_setup/templates/cloud-init.yml.j2

I would recommend to just edit whatever is between "Starting setup.sh script" and "Ending setup.sh script".
That's is a list of commands part of a BASH script that will get created and  executed on the server as last task after building the server.
The script will take care of the automation, waiting for it to finish before executing custom tasks.

  - content: |
        #!/bin/bash
        until [[ -f /var/lib/cloud/instance/boot-finished ]]; do
          sleep 5
        done
        echo "Starting setup.sh script" | logger
        >>>>> INSERT YOUR STEPS HERE <<<<<<<
        echo "Ending setup.sh script" | logger
    path: /root/setup.sh

EOF
}

## ------------------------------------------------------------


P "Installing required packages."
yum -y install python python-devel python-pip git gcc
#yum -y install python python-devel python-setuptools git gcc
#easy_install pip
#pip install --upgrade pip
pip install httpie

P "Create the 'autoscale' user, with home directory set into /opt/autoscale"
adduser -d /opt/autoscale autoscale
mkdir -p /opt/autoscale/.ssh

P "Create ssh-key for 'autoscale' user.
This key will be stored into SSH KEY section under the customer's Cloud Control Panel.
It will be pushed using autoscale_setup python script in the next steps.
It will be automatically installed on any new server built by Rackspace Autoscale via rax-autoscaler
allowing 'autoscale' user on this admin server to ssh as 'root' to the new nodes"
ssh-keygen -t rsa -f /opt/autoscale/.ssh/id_rsa -q -N ""

P "Create a new ssh key for the cloud-init phase.
This key will be the id_rsa key of root on all the new servers built by Rackspace Autoscale via rax-autoscaler,
and will allow the 'root' user on the new servers to connect passwordless to the admin node and pull the content set into the cloud-init script (e.g. playbooks)"
ssh-keygen -t rsa -f /opt/autoscale/.ssh/cloudinitsshkey -q -N ""

P "Add the new pub key into the admin server in order to allow connections from the new servers"
# Also set "StrictHostKeyChecking no" as default options ONLY for autoscale user to avoid to accept the keyprint 
# every time you try to connect as root to a new host in the autoscale group
cat /opt/autoscale/.ssh/cloudinitsshkey.pub >> /opt/autoscale/.ssh/authorized_keys
chmod 600 /opt/autoscale/.ssh/authorized_keys
echo "StrictHostKeyChecking no" >> /opt/autoscale/.ssh/config
chmod 600 /opt/autoscale/.ssh/config

P "Create folder where to keep the playbooks and set the environment to use 'localhost'.
NOTE: playbooks will be pulled by cloud-init from the 'admin' node and run LOCALLY onto each new Cloud Server created."
mkdir -p /opt/autoscale/payload/playbook/
echo "localhost ansible_connection=local" > /opt/autoscale/payload/playbook/hosts

# Install autoscale_setup and rax-autoscaler scripts
cd /opt/autoscale

P "Install autoscale_setup script"
inst_autoscale-setup

P "Install rax-autoscaler script"
inst_rax-autoscaler

# Copying required scripts in a default location for easy access.
# 'payload' dir is generally sync'd onto new autoscale nodes
#
# 'add_self_to_lb.py' into the /opt/autoscale folder allows new nodes to add \
# themselves to the load balancer once all the config has finished (no CLB set into autoscale group)
# 'autoscale.py' is the script used by rax-autoscaler to query if scaling or not
# 'remove_dead_nodes.py' is used to clean up dead nodes from the autoscale group
# 'rax-autoscaler_wrapper.sh' is used to notify customer if something gets stuck in
# rax-autoscaler and take manual intervention, with 'fail_file_monitor.py'.
cp /opt/autoscale/autoscale_setup/load_balancing/add_self_to_lb.py /opt/autoscale/payload/
cp /opt/autoscale/rax-autoscaler/contrib/autoscale.py /opt/autoscale/payload/
cp /opt/autoscale/autoscale_setup/load_balancing/remove_dead_nodes.py /opt/autoscale/
cp /opt/autoscale/autoscale_setup/monitoring/wrapper.sh /opt/autoscale/rax-autoscaler_wrapper.sh
mkdir -p /usr/lib/rackspace-monitoring-agent/plugins/
cp /opt/autoscale/autoscale_setup/monitoring/fail_file_monitor.py /usr/lib/rackspace-monitoring-agent/plugins/
chmod +x /opt/autoscale/payload/add_self_to_lb.py
chmod +x /opt/autoscale/payload/autoscale.py
chmod +x /opt/autoscale/remove_dead_nodes.py
chmod +x /opt/autoscale/rax-autoscaler_wrapper.sh
chmod +x /usr/lib/rackspace-monitoring-agent/plugins/fail_file_monitor.py

# Resetting some defaults for autoscale.py
fix_autoscale_py

# Setting CLBs into 'add_self_to_lb.py' and 'remove_dead_nodes.py' scripts
sed -i "s/^lbs.*$/lbs = [$LBS]/" /opt/autoscale/payload/add_self_to_lb.py
sed -i "s/^lbs.*$/lbs = [$LBS]/" /opt/autoscale/remove_dead_nodes.py

# Fixing logging setup and permissions
sed -i '/\[handler_fileHandler\]/,/^\[/ s/\(args=.*\)logging\.log\(.*$\)/\1\/var\/log\/rax-autoscaler\/logging\.log\2/' /etc/rax-autoscaler/logging.conf
chown -R autoscale:autoscale /var/log/rax-autoscaler/


P "Setting up credentials using customer's details provided"
# Create hidded .credentials file used by all our tools
echo -e "[rackspace_cloud]\nusername = $USERNAME\napi_key = $APIKEY\nregion = $REGION" > /opt/autoscale/.credentials

# Set autoscaler.ini file used by autoscale_setup to access our .credentials file and default setup
autoscale_setup_defaults
#echo -e "[cloud]\ncredentials_file = '/opt/autoscale/.credentials'" > /opt/autoscale/autoscaler.ini

# Set the script 'remove_dead_nodes.py' to use our .credentials file
sed -i "s/^credentials.*$/credentials = '\/opt\/autoscale\/\.credentials\'/" /opt/autoscale/remove_dead_nodes.py

# Creating extra help file:
extra_help

# Fixing autoscal user permissions
chown -R autoscale:autoscale /opt/autoscale

P "Pre-setting crontabs for user 'autoscale'. By default they are set as DISABLED'
Use crontab -e to enable them"
crontab -l > /dev/null 2>&1 | { cat; echo "
# RAX-AUTOSCALER CRONTABS
#*/2 * * * * /opt/autoscale/rax-autoscaler_wrapper.sh
#*/5 * * * * /opt/autoscale/remove_dead_nodes.py > /dev/null 2>&1
"; } | crontab -u autoscale -


# Print next actions
P "
================
What's left now?
================

1) Become 'autoscale' user
# su - autoscale

2) Edit the monitoring script file and set parameters:
~ vim /opt/autoscale/payload/autoscale.py

3) Review the cloud-init template 
~ vim /opt/autoscale/autoscale_setup/templates/cloud-init.yml.j2

4) Run the main setup script to set your autoscale group
~ cd autoscale_setup
~ ./main.py
You can use the file /opt/autoscale/autoscale_setup/extra_help_for_me.txt for further clarifications.

5) Set MAXFAILURES variable in /opt/autoscale/rax-autoscaler_wrapper.sh

6) Test and enable the crons under autoscale user
# crontab -u autoscale -e

HAPPY AUTOSCALING!!!
"

